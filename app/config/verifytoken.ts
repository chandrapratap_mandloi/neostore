import jwt from 'jsonwebtoken'

const verifyToken = function(req:any,res:any,next:any){
    const bearerHeader = req.headers['authorization'];
    const token = bearerHeader;
    req.token = token;
    jwt.verify(token, 'privateKey', function(err:any, authData:any) {
      if(err) {
        // return res.sendStatus(403);
        res.status(404).json({success:false,status:404,message:"Token Not Verified",error_message:err})
      }
      else{
        req.body._id = authData._id;
        next();
      }
    });
}
export default verifyToken
