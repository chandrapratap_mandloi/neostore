//Api for adding new color
import colormodel from '../../models/colormodel'
import {Request,Response} from 'express'

const color = (req:Request,res:Response)=>{

    /**
     * @param color_name  name of color
     * @param color_code color code
     */
    let newData = new colormodel({
        color_name:req.body.color_name,
        color_code:req.body.color_code
    })
    newData.save()
    .then(result=>{
        res.status(200).json({success:true,status:200,message:"Color Added",color:result})
    })
    .catch(err=>{
        res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
    })
}
export default color
