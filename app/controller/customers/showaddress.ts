import addressmodel from '../../models/addressmodel'
import {Request,Response} from 'express'

const showaddress = (req:Request,res:Response)=>{
    
    /**
     * @param _id id of user from token
     * @returns returns all address of user
     */
    addressmodel.find({"customer_id":req.body._id})
    .then(result=>{
        res.status(200).json({success:true,status:200,message:"User Address are:",customer:result})
        console.log('result',result)
        console.log('_id',req.body._id)
    })
    .catch(err=>{
        res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
    })
}
export default showaddress