import categorymodel from '../../models/categorymodel'
import {Request,Response} from 'express'


const allcategory = (req:Request,res:Response)=>{

    /**
     * @returns details of all category
     */
    categorymodel.find()
    .then(result=>{
        res.status(200).json({success:true,status:200,product:result})
    })
    .catch(err=>{
        res.status(404).json({success:false,status:404,err:err.details});
    })
    
}
export default allcategory
