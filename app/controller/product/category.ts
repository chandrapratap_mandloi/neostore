// api for adding new category
import categorymodel from '../../models/categorymodel'
import {Request,Response} from 'express'

const category = (req:Request,res:Response)=>{
    
    /**
     * @param category_name name of category
     * @param category_image common image of particular category
     */

    let newData = new categorymodel({
        category_name:req.body.category_name,
        category_image:req.file.filename
    })
    
    newData.save()
    .then(result=>{
        res.status(200).json({success:true,status:200,message:"Category Added",category:result})
    })
    .catch(err=>{
        res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
    })
}
export default category