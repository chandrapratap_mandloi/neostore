// this api is used for finding particular product
import productmodel from '../../models/productmodel'
import {Request,Response} from 'express'

const findproduct = (req:Request,res:Response)=>{

    /**
     * @param _id id of product to find
     */
    productmodel.find({"_id":req.body._id})
    .populate('category_id')
    .populate('color_id')
    .then(result=>{
        if(result.length==0)
        {
           res.status(404).json({success:false,message:'No Product Found'})
        }
        else{
            res.status(200).json({success:true,status:200,product:result})
        }
    })
    .catch(err=>{
        res.status(404).json({success:false,status:404,err:err.details});
    })
    
}
export default findproduct