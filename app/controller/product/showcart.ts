import cartmodel from '../../models/cartmodel'
import {Request,Response} from 'express'

const showcart = (req:Request,res:Response)=>{
    
    cartmodel.find({"user_id":req.body._id,flag:false}).select('-flag')
    .populate('product_id')
    .then(result=>{
        res.status(200).json({success:true,status:200,length:result.length,message:"Cart Details",cart:result})
    })
    .catch(err=>{
        res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
    })
}
export default showcart