// this api is used for showing toprated products of each category to display on dashboard 
import productmodel from '../../models/productmodel'
import {Request,Response} from 'express'

const showproducts = (req:Request,res:Response)=>{
    /*
        sort products according to rating in descending order
        grouped them according to category
        then push first element from each group to array of product
    */
    productmodel.aggregate([
       ([{
        $lookup:
        {
            from: "colors",
            localField: "color_id",
            foreignField: "color_id",
            as: "color"
        }
    },
            { 
                $sort: 
                { product_rating: -1} 
            },
            { $group: 
                { 
                    _id: "$category_id",
                    // products:{$push:"$$ROOT"}
                    products:{$first:"$$ROOT"}    
                } 
            }
        ]) 
    ])
    .then(result=>{
        res.status(200).json({success:true,status:200,message:"Top rated products are",customer:result})
    })
    .catch(err=>{
        res.status(404).json({success:false,status:404,message:"Something went wrong",error_message:err})
    })
}
export default showproducts