import mongoose from 'mongoose'
const Schema = mongoose.Schema
 const cart = new Schema({
     user_id:{
        type:String,
        ref:"login"
     },
    product_id:{
        type: String,
        ref:"product"
    },
    qty:{
        type: Number,
        required: true
    },
    total:{
        type: Number,
        // required: true
    },
    subtotal:{
        type: Number,
        default:0
    },
    gst:{
        type: Number,
        // required: true
    },
    ordertotal:{
        type: Number,
        // required: true
    },
    flag:{
        type:String,
        default:false
    },
    price:{
        type:Number
    },
    orderno:{
        type:String
    },
    orderplaced:{
        type : Date, 
        default: Date.now 
    }
 })

const cartmodel = mongoose.model('cart',cart)
export default cartmodel