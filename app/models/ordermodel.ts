import mongoose from 'mongoose'
import { string } from 'joi';
const Schema = mongoose.Schema
 const order = new Schema({
     user_id:{
        type:String,
        ref:"login"
     },
    product_id:{
        type: String,
        ref:"product"
    },
    address_id:{
        type: String,
        ref:"address",
        required:"true"
    },
    qty:{
        type: Number,
        required: true
    },
    total:{
        type: Number,
        // required: true
    },
    subtotal:{
        type: Number,
        // required: true
    },
    gst:{
        type: Number,
        // required: true
    },
    ordertotal:{
        type: Number,
        // required: true
    },
    flag:{
        type:String,
        default:false
    },
    price:{
        type:Number
    },
    orderno:{
        type:String
    },
    orderplaced:{
        type : Date, 
        default: Date.now 
    },
    address:{type:String},
    city:{type:String},
    state:{type:String},
    country:{type:String},
    pincode:{type:Number},
 })

const ordermodel = mongoose.model('order',order)
export default ordermodel