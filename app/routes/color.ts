
import color from '../controller/color/color'
import getcolor from '../controller/color/getcolor'
import allcolor from '../controller/color/allcolors'
import verifyToken from '../config/verifytoken';
export class Color{

    public routes(app:any):void{
        app.route('/color')
        .post(color)

        app.route('/getcolor')
        .post(getcolor)

        app.route('/colors')
        .get(allcolor)

    }
}


// app.route(upload.single("prod_image"),newProduct)
