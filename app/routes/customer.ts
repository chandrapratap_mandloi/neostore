import registerUser from '../controller/customers/customer'
import login from '../controller/customers/login'
import editprofile from '../controller/customers/profile'
import upload from '../config/multer'
import verifyToken from '../config/verifytoken'
import getuser from '../controller/customers/getuser'
import addaddress from '../controller/customers/address'
import showaddress from '../controller/customers/showaddress'
import editaddress from '../controller/customers/editaddress'
import deleteaddress from '../controller/customers/delteaddress'
import token from '../controller/customers/verifytoken'
import contactus from '../controller/customers/contactus'
import invoice from '../controller/html'
import SendReport from '../controller/customers/SendReport'
export class user{

    public routes(app:any):void{

        app.route('/registerUser')
        .post(registerUser)


        app.route('/login')
        .post(login)

        app.route('/contactus')
        .post(contactus)
        
        app.route('/editprofile')
        .post(upload.single('profile_img'),verifyToken,editprofile)

        // app.route('/editprofile')
        // .post(verifyToken,editprofile)

        app.route('/getuser')
        .get(verifyToken,getuser)

        app.route('/address')
        .post(verifyToken,addaddress)

        app.route('/showaddress')
        .get(verifyToken,showaddress)

        app.route('/editaddress')
        .post(verifyToken,editaddress)
        
        app.route('/deleteaddress/:address_id')
        .delete(verifyToken,deleteaddress)

        app.route('/token')
        .get(verifyToken,token)

        app.route('/SendReport')
        .post(SendReport)
        // app.route('/invoice')
        // .get(invoice)
    }
}