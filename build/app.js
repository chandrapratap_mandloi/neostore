"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var body_parser_1 = __importDefault(require("body-parser"));
var customer_1 = require("./routes/customer");
var product_1 = require("./routes/product");
var color_1 = require("./routes/color");
var swagger_ui_express_1 = __importDefault(require("swagger-ui-express"));
var swagger_json_1 = __importDefault(require("./swagger.json"));
var path_1 = __importDefault(require("path"));
var cors_1 = __importDefault(require("cors"));
var Application = /** @class */ (function () {
    function Application() {
        this.routeCustomer = new customer_1.user();
        this.routeProduct = new product_1.Product();
        this.routeColor = new color_1.Color();
        this.app = express_1.default();
        this.config();
        this.routeCustomer.routes(this.app);
        this.routeProduct.routes(this.app);
        this.routeColor.routes(this.app);
    }
    Application.prototype.config = function () {
        var publicDir = path_1.default.join(__dirname, '../uploads');
        var Dir = path_1.default.join(__dirname, '../invoice');
        this.app.use(express_1.default.static(publicDir));
        this.app.use(express_1.default.static(Dir));
        this.app.use(body_parser_1.default.json());
        this.app.use(cors_1.default());
        this.app.use(body_parser_1.default.urlencoded({ extended: false }));
        this.app.use('/swagger', swagger_ui_express_1.default.serve, swagger_ui_express_1.default.setup(swagger_json_1.default));
    };
    return Application;
}());
exports.default = new Application().app;
//# sourceMappingURL=app.js.map