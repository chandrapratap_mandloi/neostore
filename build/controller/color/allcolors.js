"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// Api for getting all colors
var colormodel_1 = __importDefault(require("../../models/colormodel"));
var allcolor = function (req, res) {
    colormodel_1.default.find()
        .then(function (result) {
        res.status(200).json({ success: 'true', status: 200, colors: result });
    })
        .catch(function (err) {
        res.status(404).json({ success: 'false', status: 404, err: err.details });
    });
};
exports.default = allcolor;
//# sourceMappingURL=allcolors.js.map