"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// api for getting specific color details
var productmodel_1 = __importDefault(require("../../models/productmodel"));
var getcolor = function (req, res) {
    /**
     * @param color_id
     */
    productmodel_1.default.find({ "color_id": req.body.color_id })
        .populate('color_id')
        .then(function (result) {
        res.status(200).json({ success: 'true', status: 200, product: result });
    })
        .catch(function (err) {
        res.status(404).json({ success: 'false', status: 404, err: err.details });
    });
};
exports.default = getcolor;
//# sourceMappingURL=getcolor.js.map