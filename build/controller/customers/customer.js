"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// api for registring new user
var usermodel_1 = __importDefault(require("../../models/usermodel"));
var joi_1 = __importDefault(require("joi"));
var bcrypt_1 = __importDefault(require("bcrypt"));
var sendmail_1 = __importDefault(require("sendmail"));
// var client = new twilio('TWILIO_ACCOUNT_SID', 'TWILIO_AUTH_TOKEN');
var registerUser = function (req, res) {
    /**
     * @param email
     * @param password     password must be minimum 4 characters long and 8 maximum characters long and should contains at least one capital letter, one small letter and one digit
     * @param cnfpassword  confirm password
     * @param gender       numeric in form of 0 or 1, 1 for male and 0 for female
     * @param phone
     * @param first_name
     * @param last_name
     */
    var schema = joi_1.default.object().keys({
        email: joi_1.default.string().email({ minDomainAtoms: 2 }).required(),
        password: joi_1.default.string().regex(/^(?=.*[a-zA-Z])(?=.*[0-9])/).min(4).max(8).required(),
        cnfpassword: joi_1.default.string().equal(joi_1.default.ref('password')).required(),
        gender: joi_1.default.string().required(),
        phone: joi_1.default.number().required(),
        first_name: joi_1.default.string().required(),
        last_name: joi_1.default.string().required(),
    });
    joi_1.default.validate({ email: req.body.email, password: req.body.password, cnfpassword: req.body.cnfpassword, gender: req.body.gender, phone: req.body.phone, first_name: req.body.first_name, last_name: req.body.last_name }, schema, function (err, result) {
        if (err) {
            return res.status(404).json({ success: false, status: 404, message: "Something went wrong", error_message: err });
        }
        else {
            //  for converting password to hash and send welcome mail to user
            var saltRounds = 10;
            var hash = req.body.password;
            var salt = bcrypt_1.default.genSaltSync(saltRounds);
            var passwordHsh = bcrypt_1.default.hashSync(hash, salt);
            console.log('hashed', passwordHsh);
            console.log("email:", req.body.email);
            // client.messages.create({
            //     to: req.body.phone,
            //     from: '8319462105',
            //     body: 'Hello from Neostore!'
            //   });
            var newData = new usermodel_1.default({
                email: req.body.email,
                password: passwordHsh,
                cnfpassword: req.body.cnfpassword,
                gender: req.body.gender,
                phone: req.body.phone,
                first_name: req.body.first_name,
                last_name: req.body.last_name,
            });
            newData.save()
                .then(function (result) {
                var emailSender = sendmail_1.default({
                    silent: false
                });
                var sendEmail = function (options) {
                    return new Promise(function (resolve, reject) {
                        emailSender(options, function (err, reply) {
                            if (err || !reply.startsWith("2")) {
                                reject(err);
                            }
                            else {
                                resolve(true);
                            }
                        });
                    });
                };
                var output = "<h3> Hi " + req.body.email + " You are successfully registered with NeoStore: </h3>\n                    <img src=\"uploads/neosoft.png\">\n                    ";
                sendEmail({
                    from: 'chandrapratap.mandloi@neosofttech.com',
                    to: req.body.email,
                    subject: 'Welcome to NeoStore',
                    html: output,
                });
                return res.status(200).json({ success: true, status: 200, message: "Customer Registered Successfully", user: result });
            })
                .catch(function (err) {
                usermodel_1.default.find({ email: req.body.email })
                    .then(function (result) {
                    return res.status(404).json({ success: false, status: 404, message: "Email already exist" });
                })
                    .catch(function (err) {
                    return res.status(404).json({ success: false, status: 404, message: "Something went wrong", error_message: err });
                });
            });
        }
    });
};
exports.default = registerUser;
//# sourceMappingURL=customer.js.map