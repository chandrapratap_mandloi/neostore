"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// api for edit address of user
var addressmodel_1 = __importDefault(require("../../models/addressmodel"));
var editaddress = function (req, res) {
    /**
     * @param _id  id of user from token
     * @param address
     * @param pincode
     * @param city
     * @param state
     * @param country
     * @returns updates address of user
     */
    addressmodel_1.default.update({ customer_id: req.body._id, _id: req.body.address_id }, {
        $set: {
            'address': req.body.address,
            'pincode': req.body.pincode,
            'city': req.body.city,
            'state': req.body.state,
            'country': req.body.country
        }
    })
        .then(function (result) {
        addressmodel_1.default.find({ _id: req.body.address_id })
            .then(function (result) {
            res.status(200).json({ success: true, status: 200, message: "Address Updated", address: result });
        })
            .catch(function (err) {
            res.status(404).json({ success: false, status: 404, message: "Something went wrong", error_message: err });
        });
    })
        .catch(function (err) {
        res.status(404).json({ success: false, status: 404, message: "Something went wrong", error_message: err });
    });
};
exports.default = editaddress;
//# sourceMappingURL=editaddress.js.map