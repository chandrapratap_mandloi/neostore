"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var usermodel_1 = __importDefault(require("../../models/usermodel"));
var joi_1 = __importDefault(require("joi"));
var bcrypt_1 = __importDefault(require("bcrypt"));
var jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
var addressmodel_1 = __importDefault(require("../../models/addressmodel"));
var cartmodel_1 = __importDefault(require("../../models/cartmodel"));
var login = function (req, res) {
    var schem = joi_1.default.object().keys({
        email: joi_1.default.string().email({ minDomainAtoms: 2 }).required(),
        password: joi_1.default.string().min(4).required(),
        customer_id: joi_1.default.string()
    });
    joi_1.default.validate(req.body, schem, function (err, result) {
        if (err) {
            return res.status(404).json({ success: false, status: 404, message: "Something went wrong", error_message: err });
        }
        else {
            usermodel_1.default.find({ "email": req.body.email }, function (err, result) {
                if (err) {
                    res.status(204).json({ success: 'false', status: 204, err: err.details });
                }
                if (result.length == 0) {
                    res.status(404).json({ success: 'false', status: 404, message: "Enter Correct E-Mail" });
                }
                else {
                    /**
                     * @param email
                     * @param password
                     * @returns details of user and address of user
                     */
                    var _a = result[0].toObject(), password = _a.password, users_1 = __rest(_a, ["password"]);
                    if (bcrypt_1.default.compareSync(req.body.password, password)) {
                        jsonwebtoken_1.default.sign({ email: req.body.email, _id: result[0]._id }, 'privateKey', function (err, token) {
                            addressmodel_1.default.find({ "customer_id": users_1._id })
                                .then(function (result) {
                                console.log(result, "result");
                                console.log(result[0].customer_id, "result[0]._id");
                                cartmodel_1.default.find({ user_id: result[0].customer_id, flag: false }).select('-flag')
                                    .populate('product_id')
                                    .then(function (results) {
                                    res.status(200).json({ success: true, status: 200, Message: "Login Successfully", token: token, cart_count: results.length, cart: results, data: users_1, address: result });
                                })
                                    .catch(function (err) {
                                    res.status(404).json({ success: false, status: 404, message: "Something went wrong", error_message: err });
                                });
                            })
                                .catch(function (err) {
                                res.status(404).json({ success: false, status: 404, message: "Something went wrong", error_message: err });
                            });
                        });
                    }
                    else {
                        res.status(404).json({ success: 'false', status: 404, message: "Password not Match" });
                    }
                }
            });
        }
    });
};
exports.default = login;
//# sourceMappingURL=login.js.map