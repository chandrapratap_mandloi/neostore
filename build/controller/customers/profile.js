"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
//api for editing profile of user
var usermodel_1 = __importDefault(require("../../models/usermodel"));
var editprofile = function (req, res) {
    /**
     * @param first_name
     * @param last_name
     * @param gender
     * @param dob
     * @param phone
     * @param email
     * @param profile_img
     * @returns update profile of user
     */
    if (!req.file) {
        console.log('not file');
        return res.status(404).json({ success: false, message: "Add profile picture" });
    }
    else {
        usermodel_1.default.findByIdAndUpdate(req.body._id, { $set: { first_name: req.body.first_name, last_name: req.body.last_name, dob: req.body.dob, phone: req.body.phone, gender: req.body.gender, email: req.body.email, profile_img: req.file.filename } })
            // ,'profile_img':req.file.filename
            .then(function (result) {
            var updated = { first_name: req.body.first_name, last_name: req.body.last_name, dob: req.body.dob, phone: req.body.phone, gender: req.body.gender, email: req.body.email, profile_img: req.file.filename };
            console.log(updated);
            res.status(200).json({ success: true, status: 200, message: "Profile Updated", customer: updated });
        })
            .catch(function (err) {
            res.status(404).json({ success: false, status: 404, message: "Something went wrong", error_message: err });
        });
    }
};
exports.default = editprofile;
//# sourceMappingURL=profile.js.map