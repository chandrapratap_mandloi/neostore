"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var addressmodel_1 = __importDefault(require("../../models/addressmodel"));
var showaddress = function (req, res) {
    /**
     * @param _id id of user from token
     * @returns returns all address of user
     */
    addressmodel_1.default.find({ "customer_id": req.body._id })
        .then(function (result) {
        res.status(200).json({ success: true, status: 200, message: "User Address are:", customer: result });
        console.log('result', result);
        console.log('_id', req.body._id);
    })
        .catch(function (err) {
        res.status(404).json({ success: false, status: 404, message: "Something went wrong", error_message: err });
    });
};
exports.default = showaddress;
//# sourceMappingURL=showaddress.js.map