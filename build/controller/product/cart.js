"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var cartmodel_1 = __importDefault(require("../../models/cartmodel"));
var ordermodel_1 = __importDefault(require("../../models/ordermodel"));
var productmodel_1 = __importDefault(require("../../models/productmodel"));
var cart = function (req, res) { return __awaiter(_this, void 0, void 0, function () {
    var data_1, newdata_1, unique, mystr, subtotal, ordertotal, i, result, total, newData, saveData, updateTotal, _loop_1, i_1, err_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 8, , 9]);
                data_1 = req.body;
                newdata_1 = req.body.data;
                console.log(req.body.flag);
                if (!(req.body.flag == true)) return [3 /*break*/, 6];
                unique = Date.now();
                mystr = "ORDER_";
                mystr = mystr + unique;
                console.log('mystr', mystr);
                subtotal = 0;
                ordertotal = 0;
                i = 0;
                _a.label = 1;
            case 1:
                if (!(i < newdata_1.length)) return [3 /*break*/, 5];
                return [4 /*yield*/, productmodel_1.default.find({ _id: newdata_1[i]._id })];
            case 2:
                result = _a.sent();
                total = parseInt(result[0].product_cost) * newdata_1[i].qty;
                ordertotal = ordertotal + newdata_1[i].qty * parseInt(result[0].product_cost);
                newData = new ordermodel_1.default({
                    product_id: newdata_1[i]._id,
                    user_id: req.body._id,
                    qty: newdata_1[i].qty,
                    price: newdata_1[i].product_cost,
                    total: total,
                    address_id: req.body.address_id,
                    orderno: mystr
                });
                return [4 /*yield*/, newData.save()
                        .then(function (result) {
                        cartmodel_1.default.deleteOne({ user_id: req.body._id, product_id: newdata_1[i].product_id })
                            .then();
                    })];
            case 3:
                saveData = _a.sent();
                _a.label = 4;
            case 4:
                i++;
                return [3 /*break*/, 1];
            case 5:
                ordertotal = ordertotal + (ordertotal * 0.05);
                console.log('200205050', ordertotal);
                updateTotal = ordermodel_1.default.updateMany({ user_id: req.body._id, orderno: mystr }, { ordertotal: ordertotal, flag: true })
                    .then();
                // setTimeout(dd=>{
                //   ordertotal = ordertotal+(ordertotal*0.05)
                //   console.log('200205050',ordertotal)
                //  let updateTotal = ordermodel.updateMany({user_id:req.body._id},{ordertotal:ordertotal,flag:true})
                // },2000)
                res.status(200).json({ success: true, status: 200, message: "order placed" });
                return [3 /*break*/, 7];
            case 6:
                console.log('else logout');
                _loop_1 = function (i_1) {
                    cartmodel_1.default.find({ user_id: req.body._id, product_id: data_1[i_1].product_id })
                        .then(function (result) {
                        if (result.length == 0) {
                            console.log('end');
                            console.log('i values', i_1);
                            console.log(data_1[i_1]);
                            var newData = new cartmodel_1.default({
                                product_id: data_1[i_1]._id,
                                user_id: req.body._id,
                                qty: data_1[i_1].qty,
                            });
                            var saveData = newData.save();
                        }
                        else {
                            cartmodel_1.default.update({ user_id: req.body._id, product_id: data_1[i_1]._id }, { qty: data_1[i_1].qty })
                                .then();
                        }
                    });
                };
                // let subtotal = 0;
                // let ordertotal =0
                for (i_1 = 0; i_1 < data_1.length; i_1++) {
                    _loop_1(i_1);
                }
                res.status(200).json({ success: true, status: 200, message: "Cart saved" });
                _a.label = 7;
            case 7: return [3 /*break*/, 9];
            case 8:
                err_1 = _a.sent();
                res.status(404).json({ success: false, status: 404, message: "Something went wrong", error_message: err_1 });
                return [3 /*break*/, 9];
            case 9: return [2 /*return*/];
        }
    });
}); };
exports.default = cart;
//# sourceMappingURL=cart.js.map