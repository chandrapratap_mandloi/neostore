"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// api for adding new category
var categorymodel_1 = __importDefault(require("../../models/categorymodel"));
var category = function (req, res) {
    /**
     * @param category_name name of category
     * @param category_image common image of particular category
     */
    var newData = new categorymodel_1.default({
        category_name: req.body.category_name,
        category_image: req.file.filename
    });
    newData.save()
        .then(function (result) {
        res.status(200).json({ success: true, status: 200, message: "Category Added", category: result });
    })
        .catch(function (err) {
        res.status(404).json({ success: false, status: 404, message: "Something went wrong", error_message: err });
    });
};
exports.default = category;
//# sourceMappingURL=category.js.map