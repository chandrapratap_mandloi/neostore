"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var cartmodel_1 = __importDefault(require("../../models/cartmodel"));
var checkout = function (req, res) {
    var unique = Date.now();
    var mystr = "ORDER_";
    mystr = mystr + unique;
    cartmodel_1.default.updateMany({ $and: [
            { user_id: req.body._id }, { flag: false }
        ] }, {
        $set: {
            'flag': true,
            'orderno': mystr
        }
    })
        .then(function (result) {
        res.status(200).json({ success: true, status: 200, message: "Update Cart", customer: result });
    })
        .catch(function (err) {
        res.status(404).json({ success: false, status: 404, message: "Something went wrong", error_message: err });
    });
};
exports.default = checkout;
//# sourceMappingURL=checkout.js.map