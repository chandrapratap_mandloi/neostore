"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
//  this api is used for sorting products according to category, color,  price and rating
var productmodel_1 = __importDefault(require("../../models/productmodel"));
var combine = function (req, res) {
    var _a;
    /**
     * @param category_id to sort products according to category id
     * @param color_id to sort products according to color id
     * @param sortBy to sort product according to rating or price(sortBy=product_rating)
     * @param desc   shows the way of sorting product (sortBy=product_price&desc=false) will sort product according to price in descending order
     * @param text text to find product
     */
    var a = {};
    var _b = req.query, _c = _b.sort, sort = _c === void 0 ? '{}' : _c, limit = _b.limit, page = _b.page, skip = _b.skip, sortBy = _b.sortBy, _d = _b.text, text = _d === void 0 ? '' : _d, desc = _b.desc, others = __rest(_b, ["sort", "limit", "page", "skip", "sortBy", "text", "desc"]); // destructering
    Object.entries(others).forEach(function (item) { if (item[1])
        a[item[0]] = item[1]; });
    if (text.trim()) {
        console.log(">>>>", text);
        a.product_name = { $regex: "^" + text, $options: 'i' };
    }
    // console.log(a)
    console.log(others, '==>Others');
    console.log('req.query ==>', req.query);
    productmodel_1.default.find(a).sort(sortBy ? (_a = {}, _a[sortBy] = desc == "true" ? -1 : 1, _a) : {}).limit(parseInt(req.query.limit)).skip(req.query.limit * (req.query.page - 1))
        .populate('category_id')
        .populate('color_id')
        // .count()
        .then(function (result) {
        var _a;
        productmodel_1.default.find(a).sort(sortBy ? (_a = {}, _a[sortBy] = desc == "true" ? -1 : 1, _a) : {})
            .then(function (data) {
            res.status(200).json({ success: 'true', status: 200, product: result, count: data.length });
        });
    })
        .catch(function (err) {
        res.status(404).json({ success: 'false', status: 404, err: err });
    });
};
exports.default = combine;
//# sourceMappingURL=findproducts.js.map