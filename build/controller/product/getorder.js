"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ordermodel_1 = __importDefault(require("../../models/ordermodel"));
var getorder = function (req, res) {
    /*
        first joined table using lookup
        it will show details of particular user it takes user id from token
        and group products which has same order number
    */
    ordermodel_1.default.aggregate([
        ([{
                $lookup: {
                    from: "products",
                    localField: "product_id",
                    foreignField: "product_id",
                    as: "orders"
                }
            },
            // { $match : { customer_id :req.body._id,flag:true } },
            // { $match : { user_id : req.body._id} } ,
            { $match: { user_id: req.body._id, flag: "true" } },
            // { $group : { _id : "$item" } },
            { $group: {
                    _id: "$orderno",
                    products: { $push: "$$ROOT" }
                    // products:{$first:"$$ROOT"}    
                }
            }
        ])
    ])
        .then(function (result) {
        console.log(result, '{{{{{');
        res.status(200).json({ success: true, status: 200, message: "Order Details", customer: result });
    })
        .catch(function (err) {
        res.status(404).json({ success: false, status: 404, message: "Something went wrong", error_message: err });
    });
};
exports.default = getorder;
//# sourceMappingURL=getorder.js.map