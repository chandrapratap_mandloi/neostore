"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// api is used for adding new product
var productmodel_1 = __importDefault(require("../../models/productmodel"));
var product = function (req, res) {
    /**
     * @param product_name
     * @param product_image admin can add upto 12 images of single product
     * @param prod_desc description of product
     * @param product_rating
     * @param product_producer manufacturer of product
     * @param product_cost
     * @param product_stock
     * @param product_dimension
     * @param product_material
     * @param category_id id of category to which product belongs
     * @param color_id color id of product
     */
    var images = [];
    var files = req.files;
    for (var i = 0; i < req.files.length; i++) {
        images.push(files[i].filename);
    }
    var newData = new productmodel_1.default({
        product_name: req.body.product_name,
        product_image: images,
        prod_desc: req.body.prod_desc,
        product_rating: req.body.product_rating,
        product_producer: req.body.product_producer,
        product_cost: req.body.product_cost,
        product_stock: req.body.product_stock,
        created_at: req.body.created_at,
        product_dimension: req.body.product_dimension,
        product_material: req.body.product_material,
        category_id: req.body.category_id,
        color_id: req.body.color_id
    });
    newData.save()
        .then(function (result) {
        res.status(200).json({ success: true, status: 200, message: "Product Added", Product: result });
    })
        .catch(function (err) {
        res.status(404).json({ success: false, status: 404, message: "Something went wrong", error_message: err });
    });
};
exports.default = product;
//# sourceMappingURL=product.js.map