"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var cartmodel_1 = __importDefault(require("../../models/cartmodel"));
var showcart = function (req, res) {
    cartmodel_1.default.find({ "user_id": req.body._id, flag: false }).select('-flag')
        .populate('product_id')
        .then(function (result) {
        res.status(200).json({ success: true, status: 200, length: result.length, message: "Cart Details", cart: result });
    })
        .catch(function (err) {
        res.status(404).json({ success: false, status: 404, message: "Something went wrong", error_message: err });
    });
};
exports.default = showcart;
//# sourceMappingURL=showcart.js.map