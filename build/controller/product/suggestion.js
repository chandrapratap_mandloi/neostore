"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
//  this api is used for sorting products according to category, color,  price and rating
var productmodel_1 = __importDefault(require("../../models/productmodel"));
var suggestion = function (req, res) {
    /**
     * @param category_id to sort products according to category id
     * @param color_id to sort products according to color id
     * @param sortBy to sort product according to rating or price(sortBy=product_rating)
     * @param desc   shows the way of sorting product (sortBy=product_price&desc=false) will sort product according to price in descending order
     * @param text text to find product
    */
    var a = {};
    var _a = req.query, _b = _a.sort, sort = _b === void 0 ? '{}' : _b, product_name = _a.product_name, _id = _a._id, _c = _a.text, text = _c === void 0 ? '' : _c, others = __rest(_a, ["sort", "product_name", "_id", "text"]); // destructering
    Object.entries(others).forEach(function (item) { if (item[1])
        a[item[0]] = item[1]; });
    if (text.trim()) {
        a.product_name = { $regex: "^" + text, $options: 'i' };
    }
    console.log('====>', a);
    console.log(req.query);
    console.log('others', others);
    productmodel_1.default.find(a).select('product_name')
        .populate('category_id')
        .then(function (result) {
        res.status(200).json({ success: 'true', status: 200, product: result, count: result.length });
    })
        .catch(function (err) {
        res.status(404).json({ success: 'false', status: 404, err: err });
    });
};
exports.default = suggestion;
//# sourceMappingURL=suggestion.js.map