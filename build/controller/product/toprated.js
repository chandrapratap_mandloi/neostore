"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// this api is used for showing toprated products of each category to display on dashboard 
var productmodel_1 = __importDefault(require("../../models/productmodel"));
var showproducts = function (req, res) {
    /*
        sort products according to rating in descending order
        grouped them according to category
        then push first element from each group to array of product
    */
    productmodel_1.default.aggregate([
        ([{
                $lookup: {
                    from: "colors",
                    localField: "color_id",
                    foreignField: "color_id",
                    as: "color"
                }
            },
            {
                $sort: { product_rating: -1 }
            },
            { $group: {
                    _id: "$category_id",
                    // products:{$push:"$$ROOT"}
                    products: { $first: "$$ROOT" }
                }
            }
        ])
    ])
        .then(function (result) {
        res.status(200).json({ success: true, status: 200, message: "Top rated products are", customer: result });
    })
        .catch(function (err) {
        res.status(404).json({ success: false, status: 404, message: "Something went wrong", error_message: err });
    });
};
exports.default = showproducts;
//# sourceMappingURL=toprated.js.map