"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// this api is used for updating rating of product
var productmodel_1 = __importDefault(require("../../models/productmodel"));
var updaterating = function (req, res) {
    /**
     * @param _id id of product whose rating needs to be updated
     * @param rating new rating given by user
     */
    productmodel_1.default.find({ "_id": req.body.product_id })
        .then(function (result) {
        console.log(req.body._id);
        var update;
        var reslt;
        var ratingcount = result[0].ratingcount + 1;
        update = parseFloat(result[0].rating) + parseFloat(req.body.rating);
        console.log(ratingcount);
        // update = parseFloat(result[0].product_rating) +parseFloat(req.body.rating);
        reslt = update / ratingcount;
        productmodel_1.default.findByIdAndUpdate(req.body.product_id, { $set: { 'product_rating': reslt, 'ratingcount': ratingcount, 'rating': update } })
            .then(function (result) {
            res.status(200).json({ success: 'true', status: 200, message: 'rating updated', product_rating: reslt, ratingcount: ratingcount, product: result });
        })
            .catch(function (err) {
            res.status(404).json({ success: 'false', status: 404, err: err.details });
        });
    })
        .catch(function (err) {
        res.status(404).json({ success: false, status: 404, message: "Something went wrong", error_message: err });
    });
};
exports.default = updaterating;
//# sourceMappingURL=updaterating.js.map