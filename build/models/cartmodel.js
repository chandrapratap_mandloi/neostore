"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
var Schema = mongoose_1.default.Schema;
var cart = new Schema({
    user_id: {
        type: String,
        ref: "login"
    },
    product_id: {
        type: String,
        ref: "product"
    },
    qty: {
        type: Number,
        required: true
    },
    total: {
        type: Number,
    },
    subtotal: {
        type: Number,
        default: 0
    },
    gst: {
        type: Number,
    },
    ordertotal: {
        type: Number,
    },
    flag: {
        type: String,
        default: false
    },
    price: {
        type: Number
    },
    orderno: {
        type: String
    },
    orderplaced: {
        type: Date,
        default: Date.now
    }
});
var cartmodel = mongoose_1.default.model('cart', cart);
exports.default = cartmodel;
//# sourceMappingURL=cartmodel.js.map