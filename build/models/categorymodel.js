"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
var Schema = mongoose_1.default.Schema;
var category = new Schema({
    category_name: {
        type: String,
        unique: true
    },
    category_image: {
        type: String,
        required: true
    }
});
var categorymodel = mongoose_1.default.model('category', category);
exports.default = categorymodel;
//# sourceMappingURL=categorymodel.js.map