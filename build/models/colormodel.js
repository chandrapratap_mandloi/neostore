"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
var Schema = mongoose_1.default.Schema;
var color = new Schema({
    color_name: {
        type: String,
        required: true
    },
    color_code: {
        type: String,
        required: true
    },
    color_id: {
        type: String
        // required:true
    }
});
var colormodel = mongoose_1.default.model('color', color);
exports.default = colormodel;
//# sourceMappingURL=colormodel.js.map