"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
var Schema = mongoose_1.default.Schema;
var product = new Schema({
    category_id: {
        type: String,
        ref: "category"
    },
    color_id: {
        type: String,
        ref: "color"
    },
    product_name: {
        type: String,
        required: true
    },
    product_image: {
        type: [{ type: String }],
        required: true
    },
    prod_desc: {
        type: String,
        required: true
    },
    product_rating: {
        type: Number,
        required: true,
        max: 5
    },
    product_producer: {
        type: String,
        required: true
    },
    product_cost: {
        type: Number,
        required: true
    },
    product_stock: {
        type: Number,
        required: true
    },
    product_dimension: {
        type: String,
        required: true
    },
    product_material: {
        type: String,
        required: true
    },
    ratingcount: {
        type: Number,
        default: 1
    },
    rating: {
        type: Number,
        default: 0
    }
});
var productmodel = mongoose_1.default.model('product', product);
exports.default = productmodel;
//# sourceMappingURL=productmodel.js.map