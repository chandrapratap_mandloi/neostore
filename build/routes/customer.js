"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var customer_1 = __importDefault(require("../controller/customers/customer"));
var login_1 = __importDefault(require("../controller/customers/login"));
var profile_1 = __importDefault(require("../controller/customers/profile"));
var multer_1 = __importDefault(require("../config/multer"));
var verifytoken_1 = __importDefault(require("../config/verifytoken"));
var getuser_1 = __importDefault(require("../controller/customers/getuser"));
var address_1 = __importDefault(require("../controller/customers/address"));
var showaddress_1 = __importDefault(require("../controller/customers/showaddress"));
var editaddress_1 = __importDefault(require("../controller/customers/editaddress"));
var delteaddress_1 = __importDefault(require("../controller/customers/delteaddress"));
var verifytoken_2 = __importDefault(require("../controller/customers/verifytoken"));
var user = /** @class */ (function () {
    function user() {
    }
    user.prototype.routes = function (app) {
        app.route('/registerUser')
            .post(customer_1.default);
        app.route('/login')
            .post(login_1.default);
        app.route('/editprofile')
            .post(multer_1.default.single('profile_img'), verifytoken_1.default, profile_1.default);
        // app.route('/editprofile')
        // .post(verifyToken,editprofile)
        app.route('/getuser')
            .get(verifytoken_1.default, getuser_1.default);
        app.route('/address')
            .post(verifytoken_1.default, address_1.default);
        app.route('/showaddress')
            .get(verifytoken_1.default, showaddress_1.default);
        app.route('/editaddress')
            .post(verifytoken_1.default, editaddress_1.default);
        app.route('/deleteaddress/:address_id')
            .delete(verifytoken_1.default, delteaddress_1.default);
        app.route('/token')
            .get(verifytoken_1.default, verifytoken_2.default);
        // app.route('/invoice')
        // .get(invoice)
    };
    return user;
}());
exports.user = user;
//# sourceMappingURL=customer.js.map